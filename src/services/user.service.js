export const userService = {
    login, logout
}

function login(username, password) {
    const requestOptions = {
        method: 'POST', 
        headers: { 'Content-Type': 'application/json'}, 
        body: JSON.stringify({ username, password })
    };

    return fetch('https://jsonplaceholder.typicode.com/users', requestOptions)
    .then(handleResponse)
    .then(user => {
        localStorage.setItem('user', JSON.stringify(user));
        return user;
    })
}

function logout() {
    localStorage.removeItem('user');
}