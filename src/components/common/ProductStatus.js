import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image} from 'react-native';


class ProductStatus extends Component {

    constructor(props) {
        super(props);

        this.state = [
            { title: 'Picked Up', image: '../../assets/images/pick-up.png' },
            { title: 'Scheduled', image: '../../assets/images/scheduled.png' },
            { title: 'Out of Delivery', image: '../../assets/images/out-delivery.png' },
            { title: 'Not Available', image: '../../assets/images/not-awailable.png' },
            { title: 'Delivered', image: '../../assets/images/delivered.png' },
            { title: 'Returned', image: '../../assets/images/delivered.png' }   
        ]
    }

    render() {
        return (
            <View style={styles.container}>
      
                {this.state.map((p) => {
                    return(
                        <View style={styles.item}>
                            <Text style={styles.title}>{p.title}</Text>
                            <Image resizeMode="contain" style={styles.logo} source={require('../../assets/images/pick-up.png')}/>
                        </View>
                    )}
                )}
           
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
     // alignItems: 'flex-start', // if you want to fill rows left to right,
      marginTop: 20, 
      backgroundColor: 'grey',
    alignItems: 'center',
    justifyContent: 'center'
    },
    item: {
      width: '28%', // is 50% of container width
      padding: 20,
      backgroundColor: '#fff',
      marginLeft: 20,
      textAlign: 'center', 
      marginBottom: 20, 
      
    },
    logo: {
        width: 'auto',
        height: 70,
    },
    title: {
       fontSize: 14,
       minHeight: 30,
       textAlign: 'center'
    }
  })

export default ProductStatus;