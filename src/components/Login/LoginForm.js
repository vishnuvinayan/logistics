import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity,StyleSheet, Image} from 'react-native';
import axios from 'axios';
import { connect } from 'redux';
import authReducer from '../../store/configureStore';

class LoginForm extends Component {

    constructor(props) {
        super(props);      
        this.state = {
            username: '', 
            password: '', 
            userdata: ''
        }

        this.buttonTouchHandler = this.buttonTouchHandler.bind(this);
    };

    // buttonTouchHandler = (event) => {
    //     axios.get('https://jsonplaceholder.typicode.com/users')
    //     .then(response => {
    //       console.log(response.data);
    //       this.setState({
    //         userdata: response.data
    //       })
    //       console.log('state', this.state)
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });

    // }

    buttonTouchHandler (){
        
    }
 
    render() {
        return (

            <View style={styles.ViewContainer}>
            <Image resizeMode="contain" style={styles.logo} source={require('../../../images/BFL-logo.png')} />
            <Text style={styles.text}>Logisticss</Text>

            <TextInput style = {styles.input} 
            autoCapitalize="none" 
            onSubmitEditing={() => this.passwordInput.focus()} 
            autoCorrect={false} 
            keyboardType='email-address' 
            returnKeyType="next" 
            placeholder='User Name' 
            placeholderTextColor='black'
            onChangeText={(username) => this.setState({username})}
            />

            <TextInput style = {styles.input}   
           returnKeyType="go" 
           ref={(input)=> this.passwordInput = input} 
           placeholder='Password' 
           placeholderTextColor='black' 
           onChangeText={(password) => this.setState({password})}
           secureTextEntry/>

            <TouchableOpacity style={styles.buttonContainer} >
                <Text style={styles.buttonText} onPress={this.buttonTouchHandler}>SIGN IN</Text>
            </TouchableOpacity> 

            </View>

        )
}
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
        backgroundColor: '#fff',
        borderColor: '#000',
        borderBottomWidth: 2
    },
    input:{
        height: 40,
        marginBottom: 10,
        padding: 10,
        color: 'black',
        width: 300,
        borderBottomColor: '#8D8D8D',
        borderBottomWidth: 2
    },
    buttonContainer:{
        backgroundColor: '#FDCB3D',
        paddingVertical: 15, 
        marginVertical: 55 , 
        width: 300
    },
    buttonText:{
        color: 'black',
        textAlign: 'center',
        fontWeight: '700',  
    },
    ViewContainer:{
        flex: 1, 
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    }, 
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingBottom: 20
    },
    logo: {
        width: 250,
        height: 100,
        marginBottom: 30
    }
}
);

const mapStateToProps = state => {
    return {
      username: state.authReducer.username,
      password: state.authReducer.password
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
      onLogin: (username, password) => dispatch(login(username,password)),
      onLogout: () => dispatch(logout())
    }
  }
  
//   export default connect(mapStateToProps, mapDispatchToProps) (LoginForm);

export default LoginForm;
       
