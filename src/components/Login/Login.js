import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Button, Image, KeyboardAvoidingView        } from 'react-native';
import LoginForm from './LoginForm';


export default class Login extends Component {

   

    render() {
        return (
        <KeyboardAvoidingView behavior="padding" style={styles.container}>

                {/* <View style={styles.loginContainer}>
                    
                </View> */}

                <View style={styles.formContainer}>
                    <LoginForm />
                </View>

        </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F8F8F8',
    },
    loginContainer:{
        flex : 1,
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    
}
           );


