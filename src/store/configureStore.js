import { createStore, combineReducers } from 'redux';

import { authenticationReducer } from './reducers/authenticationReducer';

const rootReducer = combineReducers({
    authReducer: authenticationReducer
});

const configureStore = () => {
    return createStore(rootReducer);
}

export default configureStore;