import { LOGIN_REQUEST, LOGOUT } from './actionTypes';

export const login = (username, password) => {
    return{
        type: LOGIN_REQUEST,
        username: username,
        password: password
    }
}

export const logout = () => {
    return{
        type: LOGOUT
    }
}



