import React  from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from './actions';
import { functionTypeAnnotation } from '@babel/types';

class LoginPage extends Component {
    constructor(props){
        super(props);

        this.props.logout();

        this.state = {
            username: '',
            password: '', 
            submitted: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({[name]: value});
    }

    buttonTouchHandler(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        if(username && password) {
            this.props.login(username, password);
        }
    
    }

    render() {
        return (

            <View style={styles.ViewContainer}>
            <Image resizeMode="contain" style={styles.logo} source={require('../../../images/BFL-logo.png')} />
            <Text style={styles.text}>Logistics</Text>

            <TextInput style = {styles.input} 
            autoCapitalize="none" 
            onSubmitEditing={() => this.passwordInput.focus()} 
            autoCorrect={false} 
            keyboardType='email-address' 
            returnKeyType="next" 
            placeholder='User Name' 
            placeholderTextColor='black'
            />

            <TextInput style = {styles.input}   
           returnKeyType="go" 
           ref={(input)=> this.passwordInput = input} 
           placeholder='Password' 
           placeholderTextColor='black' 
           secureTextEntry/>

            <TouchableOpacity style={styles.buttonContainer} >
                <Text style={styles.buttonText} onPress={this.buttonTouchHandler}>SIGN IN</Text>
            </TouchableOpacity> 

            </View>

        )
    }
}

function mapState(state) {
    const { loggingIn} = state.authentication;
    return { logginIn};
}

const actionCreators = {
    login: userActions.login,
    logout: userActions.logout
}

const connectedLoginPage = connect(mapState, actionCreators)(LoginPage);
export { connectedLoginPage as LoginPage};