import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity,StyleSheet, Image} from 'react-native';

export default class Login extends Component {

    state = {
        username: 'a', 
        password: 'ab'
    }

    buttonTouchHandler() {
        console.log('button touched1');
        console.log('username', this.state.username);
        console.log('password', this.state.password);
    }
 
    render() {
        return (

            <View style={styles.ViewContainer}>
            <Image resizeMode="contain" style={styles.logo} source={require('../../../images/BFL-logo.png')} />
            <Text style={styles.text}>Logisticss</Text>

            <TextInput style = {styles.input} 
            autoCapitalize="none" 
            onSubmitEditing={() => this.passwordInput.focus()} 
            autoCorrect={false} 
            keyboardType='email-address' 
            returnKeyType="next" 
            placeholder='User Name' 
            placeholderTextColor='black'
            />

            <TextInput style = {styles.input}   
           returnKeyType="go" 
           ref={(input)=> this.passwordInput = input} 
           placeholder='Password' 
           placeholderTextColor='black' 
           secureTextEntry/>

            <TouchableOpacity style={styles.buttonContainer} >
                <Text style={styles.buttonText} onPress={this.buttonTouchHandler}>SIGN IN</Text>
            </TouchableOpacity> 

            </View>

        )
}
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
        backgroundColor: '#fff',
        borderColor: '#000',
        borderBottomWidth: 2
    },
    input:{
        height: 40,
        // backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
        color: 'black',
        width: 300,
        borderBottomColor: '#8D8D8D',
        borderBottomWidth: 2
    },
    buttonContainer:{
        backgroundColor: '#FDCB3D',
        paddingVertical: 15, 
        marginVertical: 55 , 
        width: 300
    },
    buttonText:{
        color: 'black',
        textAlign: 'center',
        fontWeight: '700',  
    },
    ViewContainer:{
        flex: 1, 
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    }, 
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingBottom: 20
    },
    logo: {
        // position: 'absolute',
        width: 250,
        height: 100,
        // top: 50,
        marginBottom: 30
    }
}
           );
       
