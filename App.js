/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import Login from './src/components/Login/Login';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

// type Props = {};

class App extends Component {

  state = {
  }

  render() {
    return (
      <View style={styles.container}>
          <View style={styles.loginContainer}>
              <Login />
          </View>
     </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F8F8F8',
},
loginContainer:{
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
},
logo: {
    position: 'absolute',
    width: 300,
    height: 100
}
});

// const mapStateToProps = state => {
//   return {
//     username: state.authReducer.username,
//     password: state.authReducer.password
//   }
// }

// const mapDispatchToProps = dispatch => {
//   return {
//     onLogin: (username, password) => dispatch(login(username,password)),
//     onLogout: () => dispatch(logout())
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps) (App);
export default App;